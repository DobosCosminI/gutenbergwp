<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'm/IioN/AAemdg0d9ua+N4zeherRe3310I08H2YuS6/jXi4AwO5k74eU3/ThK5DWJNIQUy8lvvXeDp00fM4a1Cw==');
define('SECURE_AUTH_KEY',  'xUB916hZcgGVrDjFVw9bC+cTHCZGWohsRjraY4ftjK08ZIcQU6rbkZJHURfKGzg40Bn3RNj/ryU7/zkKutIJEg==');
define('LOGGED_IN_KEY',    'ors53waC0BFc2p2YC94oSuI7gXWTs0i6JD+ZCny7zPyVnHJASnniP7AUIydCSn6W1URS1S3JT/6/9BYgdbdv7g==');
define('NONCE_KEY',        'Bt9aG7L3/ljbvStPhsJ3EBnji0ct3PnPLnXh8V/yHYYPSah/rrHHyKZ6v30q5Ds4fLZ4t55OZee0rBrBQ7Xyyw==');
define('AUTH_SALT',        'Iz9Rj+tCSTUnzdY5OwWtcFgC42I7CA0LOn/9Kv0847xTiIAdzHT8CpMQc+nn0fzL93Rg6hRL1bHfijHaC3xK6A==');
define('SECURE_AUTH_SALT', 'd7taqFhn9jfOUEpCCoicWpV7NMjPj0wFQnDFddP31BzVzWKeayQTHXdVu+RIpkQxQGlZIesFmrA1fmDAUGkUzw==');
define('LOGGED_IN_SALT',   'bvNlD/XHdklxkuxAPeW3V/HhstpUhYElK6qGucdLlIaeytzqT61kDG6bXmWz4F8cXeAQiAwoZgBy39s+wyYp/Q==');
define('NONCE_SALT',       'CPxp+BbYaGZ/RPR54SW4j5+6oC4VOB0wgO/PRytfb8/QdlTZDwjkM0BKSIOGeX1nNaRfxnL8D12b3KrmNY1ANg==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
