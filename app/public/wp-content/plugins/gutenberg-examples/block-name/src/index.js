import { registerBlockType } from '@wordpress/blocks';
import './style.scss';
import Edit from './edit';
import Save from './save';


registerBlockType( 'create-block/block-name', {
	edit: Edit,
	save: Save
});
