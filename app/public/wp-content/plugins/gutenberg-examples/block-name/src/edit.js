import {__} from '@wordpress/i18n';
import {
	RichText, URLInputButton, AlignmentToolbar,
	BlockControls, useBlockProps, MediaUpload, InspectorControls, PanelColorSettings
} from '@wordpress/block-editor';
import {Button, PanelBody, PanelRow, FormToggle} from '@wordpress/components';
import './editor.scss';
import classnames from 'classnames';
import btn_icon from "./icon";


const Edit = (props) => {
	const {
		attributes: {title, subtitle, newURL, alignment, mediaURL, mediaID, backgroundColor, textColor, night_mode},
		setAttributes,
	} = props;

	const blockProps = useBlockProps();


	const onChangeAlignment = newAlignment => {
		setAttributes({
			alignment: newAlignment === undefined ? 'none' : newAlignment,
		});
	};
	const onSelectImage = (media) => {
		setAttributes({
			mediaURL: media.url,
			mediaID: media.id,
		});
	};
	const onChangeTitle = title => {
		setAttributes({title});
	};

	const onChangeSubtitle = subtitle => {
		setAttributes({subtitle});
	};

	const onChangeAppURL = newURL => {
		setAttributes({newURL});
	};
	const onChangeBackgroundColor = backgroundColor => {
		setAttributes({backgroundColor});
	};
	const onChangeTextColor = textColor => {
		setAttributes({textColor});
	};

	return (
		<>
			<InspectorControls>
				<PanelColorSettings
					title={__('Panel', 'block-name')}
					colorSettings={[
						{
							value: backgroundColor,
							onChange: onChangeBackgroundColor,
							label: __('Background Color', 'block-name')
						},
						{
							value: textColor,
							onChange: onChangeTextColor,
							label: __('Text Color', 'block-name')
						}
					]}
				/>
				<PanelBody title={__('Night Mode', 'block-name')}>
					<PanelRow>
						<label htmlFor="block-name-togggle">
							{__('Night Mode', 'block-name')}
						</label>
						<FormToggle id='block-name-togggle'
									checked={night_mode}
									onClick={() => {
										setAttributes({night_mode: !night_mode})
									}}
						/>
					</PanelRow>
				</PanelBody>
			</InspectorControls>
			<BlockControls>
				<AlignmentToolbar
					value={alignment}
					onChange={onChangeAlignment}
				/>
				{/*<ToolbarGroup>*/}
				{/*	<Tooltip text={__('Night mode', 'recipe')}>*/}
				{/*		<Button className={classnames(*/}
				{/*			'components-icon-button',*/}
				{/*			'components-toolbar__control',*/}
				{/*			{'is-active': night_mode}*/}
				{/*		)}*/}
				{/*				onClick={() => {*/}
				{/*					setAttributes({night_mode: !night_mode})*/}
				{/*				}}>*/}
				{/*			{btn_icon}*/}
				{/*		</Button>*/}
				{/*	</Tooltip>*/}

				{/*</ToolbarGroup>*/}
			</BlockControls>

			<div {...blockProps}>
				<section className={classnames(
					{'night': night_mode}
				)}>
					<div className="inner">
						<span className="fa-cloud"></span>
						<RichText
							tagName="h1"
							style={{textAlign: alignment, color: textColor}}
							onChange={onChangeTitle}
							value={title}
							placeholder={__("Title", "block-name")}
						/>

						<RichText
							tagName="p"
							onChange={onChangeSubtitle}
							style={{textAlign: alignment, color: textColor}}
							value={subtitle}
							placeholder={__("Subtitle", "block-name")}

						/>
						<div className="actions special">

							<a href={newURL} className="button scrolly" target="_blank"
							   rel="noopener noreferrer">Discover</a>
							<URLInputButton
								onChange={onChangeAppURL}
								url={newURL}
							/>
						</div>

						<div className="recipe-image">
							<MediaUpload
								onSelect={onSelectImage}
								allowedTypes="image"
								value={mediaID}
								render={({open}) => (
									<Button
										className={
											mediaID ? 'image-button' : 'button button-large'
										}
										onClick={open}
									>
										{!mediaID ? (
											__('Upload Image', 'block-name')
										) : (
											<img
												src={mediaURL}
												alt={__(
													'Upload Recipe Image',
													'block-name'
												)}
											/>
										)}
									</Button>
								)}
							/>
						</div>

					</div>
				</section>
			</div>
		</>
	);
};

export default Edit;
