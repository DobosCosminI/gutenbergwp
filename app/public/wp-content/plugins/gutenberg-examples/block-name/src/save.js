import {RichText, useBlockProps} from '@wordpress/block-editor';
import classnames from "classnames";

const Save = (props) => {

	const {
		attributes: {title, subtitle, newURL, mediaURL,backgroundColor, textColor,alignment,night_mode},
	} = props;

	const blockProps = useBlockProps.save();

	return (
		<div {...blockProps}>
			<section className={classnames(
					{'night': night_mode}
			)}>
			<div className="inner">
					<span className=" fa-cloud"></span>
					<RichText.Content
						tagName="h1"
						value={title}
						style={{textAlign: alignment, color: textColor}}
					/>
					<RichText.Content
						tagName="p"
						value={subtitle}
						style={{textAlign: alignment, color: textColor}}
					/>
					<ul className="actions special">
						<li>
							<a href={newURL} className="button scrolly" target="_blank"
							   rel="noopener noreferrer">Discover</a>

						</li>
					</ul>
					<div>
						{mediaURL && (
							<img
								src={mediaURL}
							/>
						)}
					</div>
				</div>
			</section>
		</div>
	);
};

export default Save;
