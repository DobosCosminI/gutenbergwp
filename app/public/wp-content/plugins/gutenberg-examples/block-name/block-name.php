<?php
/**
 * Plugin Name:       Block Name
 * Description:       Example block written with ESNext standard and JSX support – build step required.
 * Requires at least: 5.8
 * Requires PHP:      7.0
 * Version:           0.1.0
 * Author:            The WordPress Contributors
 * License:           GPL-2.0-or-later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       block-name
 *
 * @package           create-block
 */

/**
 * Registers the block using the metadata loaded from the `block.json` file.
 * Behind the scenes, it registers also all assets so they can be enqueued
 * through the block editor in the corresponding context.
 *
 * @see https://developer.wordpress.org/block-editor/how-to-guides/block-tutorial/writing-your-first-block-type/
 */


function create_block_block_name_block_init() {
	register_block_type( __DIR__ );

	if ( function_exists( 'wp_set_script_translations' ) ) {
		wp_set_script_translations( 'block-name', 'block-name' );
	}
}
add_action( 'init', 'create_block_block_name_block_init' );

function mytheme_blocks_categories($categories, $post)
{
	return array_merge(
		$categories,
		array(
			array(
				'slug' => 'mytheme-layout',
				'title' => __('My Theme Layout', 'mytheme-blocks'),
				'icon' => 'wordpress'
			)
		)
	);
}
add_filter('block_categories', 'mytheme_blocks_categories', 10, 2);


add_action( 'wp_enqueue_scripts', 'custom_load_font_awesome',10,2 );
/**
 * Enqueue Font Awesome.
 */
function custom_load_font_awesome() {

	wp_enqueue_style( 'font-awesome-free', '//use.fontawesome.com/releases/v5.2.0/css/all.css' );

}
